﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HeThong;
using DevExpress.XtraReports.UI;

namespace HoaDon
{
    public partial class frmTaoHoaDon : Form
    {
        bool isInsert = false;
        int mhd = 0;
        DatabaseDataContext db = new DatabaseDataContext();
        List<SanPham> arrNhanVien;
        public frmTaoHoaDon()
        {
            InitializeComponent();
            arrNhanVien = new List<SanPham>();
        }
        private void frmTaoHoaDon_Load(object sender, EventArgs e)
        {
            loadView();
            LoadDV();
            barManager1.SetPopupContextMenu(gcDV, popupMenu1);
            barManagerHD.SetPopupContextMenu(gcHD, popupMenuHD);

        }
        private void loadView()
        {
            int tmp = (from a in db.HoaDon1s select a).Count() + 1;
            txtMaHD.Text = "HD" + tmp;
            txtTenHD.Text = "Hóa đơn chung cư (HD" + tmp + ")";
            lueLoaiDV.Properties.DataSource = from a in db.LoaiDVs select a;
            txtNgayTao.Text = String.Format("{0:dd/MM/yyyy  HH:mm:ss}",db.GetThoiGian());
            txtNguoiTao.Text = HeThong.Common.User.TenNV;
            lueCanHo.Properties.DataSource = from a in db.CanHos
                                             select new
                                             {
                                                 a.ID,
                                                 a.MaCH,
                                                 a.TenCH,
                                                 a.DiaChi
                                             };
        }
        private void LoadDV()
        {
            gcDV.DataSource = (from a in db.DichVus select new {
                a.ID,
                a.MaDV,
                a.TenDV,
                a.Gia,
                TenLoaiDV = (from b in db.LoaiDVs where a.IDLoaiDV == b.ID select b.TenLoaiDV).Single()
            }).ToList();
        }
        private void loadHoaDon()
        {
            gcHD.DataSource = arrNhanVien;
            gcHD.RefreshDataSource();
            txtTong.Text = tinhtongTien() + " K-VNĐ"; 
        }
        private double tinhtongTien()
        {
            double tong = 0;
            foreach(SanPham i in arrNhanVien)
            {
                tong += i.ThanhTien;
            }
            return tong;    
        }
        private void btnThemVaoHD_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //chuyển vào ds hóa đơn
            if (gvDV.GetFocusedRowCellValue("ID") == null)
            {
                Thongbao.Hoi("Mời chọn hàng dịch vụ");
            }
            else
            {
                int id_tmp = (int)gvDV.GetFocusedRowCellValue("ID");
                var vl = (from a in db.DichVus where a.ID == (int)id_tmp select a).Single();
                //ckeck có trong HD 
                foreach(SanPham i in arrNhanVien)
                {
                    if (i.ID == vl.ID) return;
                }
                arrNhanVien.Add(new SanPham(vl.ID, vl.MaDV, vl.TenDV, (double)vl.Gia, 1, (double)vl.Gia));
                loadHoaDon();
            }
        }
        private void btnLoaibo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            //loại khỏi ds hóa đơn
            if (gvHD.GetFocusedRowCellValue("ID") == null)
            {
                Thongbao.Hoi("Mời chọn hàng dịch vụ");
            }
            else
            {
                int id_tmp = (int)gvHD.GetFocusedRowCellValue("ID");
                for(int i=0;i < arrNhanVien.Count;i++)
                {
                    if (arrNhanVien[i].ID == id_tmp)
                    {
                        arrNhanVien.Remove(arrNhanVien[i]);
                    }
                }
                loadHoaDon();
            }
        }
        private void btnLoai_Click(object sender, EventArgs e)
        {
            //loại khỏi ds hóa đơn
            if (gvHD.GetFocusedRowCellValue("ID") == null)
            {
                Thongbao.Hoi("Mời chọn hàng dịch vụ");
            }
            else
            {
                int id_tmp = (int)gvHD.GetFocusedRowCellValue("ID");
                for (int i = 0; i < arrNhanVien.Count; i++)
                {
                    if (arrNhanVien[i].ID == id_tmp)
                    {
                        arrNhanVien.Remove(arrNhanVien[i]);
                    }
                }
                loadHoaDon();
            }
        }
        private void btnThemHD_Click(object sender, EventArgs e)
        {
            //chuyển vào ds hóa đơn
            if (gvDV.GetFocusedRowCellValue("ID") == null)
            {
                Thongbao.Hoi("Mời chọn hàng dịch vụ");
            }
            else
            {
                int id_tmp = (int)gvDV.GetFocusedRowCellValue("ID");
                var vl = (from a in db.DichVus where a.ID == (int)id_tmp select a).Single();
                //ckeck có trong HD 
                foreach (SanPham i in arrNhanVien)
                {
                    if (i.ID == vl.ID) return;
                }
                arrNhanVien.Add(new SanPham(vl.ID, vl.MaDV, vl.TenDV, (double)vl.Gia, 1, (double)vl.Gia));
                loadHoaDon();
            }
        }

        private void btnTang_Click(object sender, EventArgs e)
        {
            if (gvHD.GetFocusedRowCellValue("ID") == null)
            {
                Thongbao.Hoi("Mời chọn hàng dịch vụ");
            }
            else
            {
                int id_tmp = (int)gvHD.GetFocusedRowCellValue("ID");
                for (int i = 0; i < arrNhanVien.Count; i++)
                {
                    if (arrNhanVien[i].ID == id_tmp)
                    {
                        arrNhanVien[i].SoLuong++;
                        arrNhanVien[i].ThanhTien = arrNhanVien[i].SoLuong * arrNhanVien[i].Gia;
                    }
                }
                loadHoaDon();
            }
        }

        private void btnGiam_Click(object sender, EventArgs e)
        {
            if (gvHD.GetFocusedRowCellValue("ID") == null)
            {
                Thongbao.Hoi("Mời chọn hàng dịch vụ");
            }
            else
            {
                int id_tmp = (int)gvHD.GetFocusedRowCellValue("ID");
                for (int i = 0; i < arrNhanVien.Count; i++)
                {
                    if (arrNhanVien[i].ID == id_tmp)
                    {
                        if(arrNhanVien[i].SoLuong>=1)
                        arrNhanVien[i].SoLuong--;
                        arrNhanVien[i].ThanhTien = arrNhanVien[i].SoLuong * arrNhanVien[i].Gia;
                    }
                }
                loadHoaDon();
            }
        }

        private void btnTaoHD_Click(object sender, EventArgs e)
        {
            if (arrNhanVien.Count == 0)
            {
                Thongbao.Canhbao("Hóa đơn rỗng...Xin thêm dịch vụ sử dung.");
                return;
            }
            isInsert = false; 
            //tạo hd
            HoaDon1 obj = new HoaDon1();
            if (checkvali_null()) return;
            obj.MaHD = txtMaHD.Text;
            obj.TenHD = txtTenHD.Text;
            obj.IDNV = HeThong.Common.User.ID;
            obj.IDCH =(int)lueCanHo.EditValue;
            obj.NgayTao = db.GetThoiGian();
            obj.GhiChu = txtGhiChu.Text;
            obj.Tong =(decimal)tinhtongTien();
            db.HoaDon1s.InsertOnSubmit(obj);
            try
            {
                db.SubmitChanges();
                isInsert = true;
            }
            catch (Exception)
            {
                MessageBox.Show("LỖI DATABASE");
            }
            if (isInsert)
            {
                ChitietHD ob;
                try
                {
                    int id_HD = (from a in db.HoaDon1s where a.MaHD == txtMaHD.Text select a.ID).Single();
                    foreach (SanPham i in arrNhanVien)
                    {
                        ob = new ChitietHD();
                        ob.IDHD = id_HD;
                        ob.IDDV = i.ID;
                        ob.Gia =(decimal)i.Gia;
                        ob.SoLuong = i.SoLuong;
                        db.ChitietHDs.InsertOnSubmit(ob);
                    
                    }
                    db.SubmitChanges();
                    mhd = id_HD;
                    Thongbao.ThanhCong("Tạo thành công !");
                }
                catch (Exception ex)
                {
                    var delete = (from a in db.HoaDon1s where a.MaHD == txtMaHD.Text select a).Single();
                    db.HoaDon1s.DeleteOnSubmit(delete);
                    try { db.SubmitChanges(); }
                    catch (Exception) { MessageBox.Show("Xóa không thành công, vui lòng kiểm tra lại."); }
                    Thongbao.Loi("Thất bại:"+ex.ToString());
                }

            }
        }

        private bool checkvali_null()
        {
            if (String.IsNullOrEmpty(txtMaHD.Text))
            {
                Thongbao.Hoi("Mã hóa đơn. xin mời nhập !");
                return true;
            }
            if (String.IsNullOrEmpty(txtTenHD.Text))
            {
                Thongbao.Hoi("Tên hóa đơn. xin mời nhập !");
                return true;
            }
            if (lueCanHo.EditValue==null)
            {
                Thongbao.Hoi("Chưa chọn căn hộ. Xin mời chọn");
                return true;
            }
            return false;

        }

        private void lueLoaiDV_EditValueChanged(object sender, EventArgs e)
        {
            if (lueLoaiDV.EditValue == null) return;

            gcDV.DataSource = (from a in db.DichVus
                               where a.IDLoaiDV==(int)lueLoaiDV.EditValue
                               select new
                               {
                                   a.ID,
                                   a.MaDV,
                                   a.TenDV,
                                   a.Gia,
                                   TenLoaiDV = (from b in db.LoaiDVs where a.IDLoaiDV == b.ID select b.TenLoaiDV).Single()
                               }).ToList();
        }

        private void btnAll_Click(object sender, EventArgs e)
        {
            LoadDV();
        }

        private void btnInHD_Click(object sender, EventArgs e)
        {
            if (isInsert && mhd > 0)
            {

                rptHoaDon report = new rptHoaDon(mhd);
                using (ReportPrintTool printTool = new ReportPrintTool(report))
                {
                    // Invoke the Print dialog.
                    //printTool.PrintDialog();

                    // Send the report to the default printer.


                    // Send the report to the specified printer.
                    printTool.ShowPreviewDialog();
                }
            }

        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            //set lại ban đầu
            mhd = 0;
            isInsert = false;
            loadView();
            lueCanHo.EditValue = null;
            txtGhiChu.Text = null;
            arrNhanVien.Clear();
            loadHoaDon();
        }

        private void lueSoluong_ValueChanged(object sender, EventArgs e)
        {
            if (gvHD.GetFocusedRowCellValue("ID") == null)
            {
                Thongbao.Hoi("Mời chọn hàng dịch vụ");
            }
            else
            {
                if (lueSoluong.Value == null) return;
                int id_tmp = (int)gvHD.GetFocusedRowCellValue("ID");
                for (int i = 0; i < arrNhanVien.Count; i++)
                {
                    if (arrNhanVien[i].ID == id_tmp)
                    {
                        arrNhanVien[i].SoLuong=(int)lueSoluong.Value;
                        arrNhanVien[i].ThanhTien = arrNhanVien[i].SoLuong * arrNhanVien[i].Gia;
                    }
                }
                loadHoaDon();
            }
        }
    }
}
