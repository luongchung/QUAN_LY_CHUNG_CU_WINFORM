﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HoaDon
{
    public class SanPham
    {
        private int iD;
        private string maDV;
        private string tenDV;
        private double gia;
        private int soLuong;
        private double thanhTien;
       public SanPham(int ID,string MaDV,string TenDV,double Gia,int SoLuong,double ThanhTien)
        {
            this.iD = ID;
            this.maDV = MaDV;
            this.tenDV = TenDV;
            this.gia = Gia;
            this.soLuong = SoLuong;
            this.thanhTien = ThanhTien;
        }
        public int ID
        {
            get
            {
                return iD;
            }

            set
            {
                iD = value;
            }
        }
        public string TenDV
        {
            get
            {
                return tenDV;
            }

            set
            {
                tenDV = value;
            }
        }
        public string MaDV
        {
            get
            {
                return maDV;
            }

            set
            {
                maDV = value;
            }
        }
        public double Gia
        {
            get
            {
                return gia;
            }

            set
            {
                gia = value;
            }
        }
        public int SoLuong
        {
            get
            {
                return soLuong;
            }

            set
            {
                soLuong = value;
            }
        }
        public double ThanhTien
        {
            get
            {
                return thanhTien;
            }

            set
            {
                thanhTien = value;
            }
        }
    }
}
