﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Linq;
using HeThong;

namespace HoaDon
{
    public partial class rptHoaDon : DevExpress.XtraReports.UI.XtraReport
    {
        DatabaseDataContext db = new DatabaseDataContext();
        public rptHoaDon(int ID_HD)
        {
            InitializeComponent();
            //lấy thông tin hóa đơn
            HoaDon1 obj = new HoaDon1();
            obj = db.HoaDon1s.Single(p => p.ID == ID_HD);

            //binding
            xrSTT.DataBindings.Add(new XRBinding("Text", null, "STT"));
            xrMaDV.DataBindings.Add(new XRBinding("Text", null, "MaDV1"));
            xrTenDV.DataBindings.Add(new XRBinding("Text", null, "TenDV1"));
            xrGia.DataBindings.Add(new XRBinding("Text", null, "Gia","{0:##.}"));
            xrSoLuong.DataBindings.Add(new XRBinding("Text", null, "SoLuong"));
            xrThanhTien.DataBindings.Add(new XRBinding("Text", null, "ThanhTien"));

            var wait = HeThong.Thongbao.Loading();

            txtNV.Text = HeThong.Common.User.TenNV;
            txtNgayTao.Text = String.Format("{0:dd/MM/yyyy  HH:mm:ss}", db.GetThoiGian());
            txtCH.Text = db.CanHos.Single(p => p.ID == obj.IDCH).MaCH;
            txtMaHD.Text = obj.MaHD;
            txtTenHD.Text = obj.TenHD;
            txtGhiChu.Text = obj.GhiChu;
            txtTong.Text = String.Format("{0:##.}",obj.Tong);

            try
            {
                    DataSource =(from k in db.ChitietHDs where k.IDHD == ID_HD select new
                    {
                        MaDV1 = (from a in db.DichVus where a.ID == k.IDDV select a.MaDV ).Single(),
                        TenDV1 = (from b in db.DichVus where b.ID == k.IDDV select  b.TenDV ).Single(),
                        Gia = k.Gia,
                        SoLuong = k.SoLuong,
                        ThanhTien=(double)k.Gia*(double)k.SoLuong
                    }).ToList();
            }
            catch { }
            finally
            {
                wait.Close();
                wait.Dispose();
            }
        }

    }
}
