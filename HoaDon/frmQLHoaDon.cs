﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HeThong;

namespace HoaDon
{
    public partial class frmQLHoaDon : Form
    {
        DatabaseDataContext db = new DatabaseDataContext();
        public frmQLHoaDon()
        {
            InitializeComponent();
        }

        private void frmQLHoaDon_Load(object sender, EventArgs e)
        {
            loadHD();
        }

        private void loadHD()
        {
            gcHD.DataSource = (from a in db.HoaDon1s
                               select new
                               {
                                   a.ID,
                                   a.MaHD,
                                   a.TenHD,
                                   MaCH1=(from b in db.CanHos where b.ID==a.IDCH select b.MaCH).ToList().Single(),
                                   a.Tong
                               }).ToList();
        }

        private void gvHD_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (gvHD.GetFocusedRowCellValue("ID") != null)
            {
                loadChiTietHd((int)gvHD.GetFocusedRowCellValue("ID"));
            }
        
        }
        private void loadChiTietHd(int id)
        {
            gcCTHD.DataSource = (from a in db.ChitietHDs
                                 where a.IDHD == id
                                 select new
                                 {
                                     a.SoLuong,
                                     a.Gia,
                                     TenDV = (from b in db.DichVus where b.ID == a.IDDV select b.TenDV ).Single()
                                 }).ToList();
        }

        private void btnXoa_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gvHD.GetFocusedRowCellValue("ID") == null)
            {
                Thongbao.Hoi("Mời bạn chọn hàng cần xóa.");
            }
            else
            {
                int id_tmp = (int)gvHD.GetFocusedRowCellValue("ID");
                DialogResult f = Thongbao._CauHoi();
                if (f == System.Windows.Forms.DialogResult.Yes)
                {
                    bool isCheck = false;
                    try
                    {
                        db.XoaChitietHD(id_tmp);
                        isCheck = true;
                    }
                    catch
                    {

                    }
                    if (isCheck)
                    {
                        var delete = (from a in db.HoaDon1s where a.ID == (int)id_tmp select a).Single();
                        db.HoaDon1s.DeleteOnSubmit(delete);
                        try { db.SubmitChanges(); }
                        catch (Exception) { MessageBox.Show("Xóa không thành công, vui lòng kiểm tra lại."); }
                    }              
                    loadHD();
                }
            }
        }

        private void btnNap_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            loadHD();
        }

        private void btnSua_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

        }
    }
}
