﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HeThong;

namespace QL_CanHo
{  
    public partial class addCanHo : DevExpress.XtraEditors.XtraForm
    {
        private DatabaseDataContext db;
        private CanHo obj;
        public int? ID { get; set; }
        public addCanHo()
        {
            InitializeComponent();
            db = new DatabaseDataContext();
            obj = new CanHo();
        }
        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (ID == null)
            {
                if (checkvali_null()) return;
                obj.MaCH = txtMaCH.Text;
                obj.TenCH = txtTenCH.Text;
                obj.DiaChi = txtDiaChi.Text;
                obj.DienTich = (decimal)txtDienTich.Value;
                db.CanHos.InsertOnSubmit(obj);
                try
                {
                    db.SubmitChanges();
                }
                catch (Exception)
                {
                    MessageBox.Show("LỖI DATABASE");
                }
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
            else
            {
                obj.MaCH = txtMaCH.Text;
                obj.TenCH = txtTenCH.Text;
                obj.DiaChi = txtDiaChi.Text;
                obj.DienTich = (decimal)txtDienTich.Value;
                try
                {
                    db.SubmitChanges();
                }
                catch (Exception)
                {
                    MessageBox.Show("LỖI DATABASE");
                }
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
        }
        private bool checkvali_null()
        {
            if (String.IsNullOrEmpty(txtTenCH.Text))
            {
                MessageBox.Show("Tên căn hộ trống. xin mời nhập !", "Thông báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                return true;
            }
            if (String.IsNullOrEmpty(txtMaCH.Text))
            {
                MessageBox.Show("Mã căn hộ trống. xin mời nhập !", "Thông báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                return true;
            }
            return false;

        }

        private void addCanHo_Load(object sender, EventArgs e)
        {
            if (ID != null)
            {
                obj = db.CanHos.Single(p => p.ID == ID);
                txtMaCH.Text = obj.MaCH;
                txtTenCH.Text = obj.TenCH;
                txtDienTich.Value = (decimal)obj.DienTich;
                txtDiaChi.Text = obj.DiaChi;
            }
        }
    }
}
