﻿using HeThong;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QL_DichVu
{
    public partial class frmaddDV : DevExpress.XtraEditors.XtraForm
    {
        public int? ID { get; set;}
        private DatabaseDataContext db;
        private DichVu obj;
        public frmaddDV()
        {
            InitializeComponent();
            db = new DatabaseDataContext();
            obj = new DichVu();
        }
        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (ID == null)
            {
                if (checkvali_null()) return;
                obj.MaDV = txtMaDV.Text;
                obj.TenDV = txtTenDV.Text;
                obj.Gia =(decimal) txtGia.Value;
                obj.IDLoaiDV = (int)lueLoaiDV.EditValue;
                db.DichVus.InsertOnSubmit(obj);
                try
                {
                    db.SubmitChanges();
                }
                catch (Exception)
                {
                    MessageBox.Show("LỖI DATABASE");
                }
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
            else
            {
                obj.MaDV = txtMaDV.Text;
                obj.TenDV = txtTenDV.Text;
                obj.Gia = (decimal)txtGia.Value;
                obj.IDLoaiDV = (int)lueLoaiDV.EditValue;
                try
                {
                    db.SubmitChanges();
                }
                catch (Exception)
                {
                    MessageBox.Show("LỖI DATABASE");
                }
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
        }



        private bool checkvali_null()
        {
            if (String.IsNullOrEmpty(txtMaDV.Text))
            {
                MessageBox.Show("Mã dịch vụ. xin mời nhập !", "Thông báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                return true;
            }
            if (String.IsNullOrEmpty(txtTenDV.Text))
            {
                MessageBox.Show("Tên dịch vụ. xin mời nhập !", "Thông báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                return true;
            }
            if (String.IsNullOrEmpty(txtGia.Value.ToString()))
            {
                MessageBox.Show("Giá dịch vụ. xin mời nhập !", "Thông báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                return true;
            }
            if (lueLoaiDV.EditValue==null)
            {
                MessageBox.Show("Chưa chọn loại dịch vụ", "Thông báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                return true;
            }
            return false;
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmaddDV_Load(object sender, EventArgs e)
        {
            lueLoaiDV.Properties.DataSource = (from a in db.LoaiDVs select new { a.ID,a.MaLoaiDV,a.TenLoaiDV }).ToList();
            if (ID != null)
            {
                obj = db.DichVus.Single(p => p.ID == ID);
                txtMaDV.Text = obj.MaDV;
                txtTenDV.Text = obj.TenDV;
                txtGia.Value =(decimal) obj.Gia;
                lueLoaiDV.EditValue =(int) obj.IDLoaiDV;
            }
        }
    }
}
