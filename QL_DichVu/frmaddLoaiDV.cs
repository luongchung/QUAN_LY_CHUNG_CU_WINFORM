﻿using HeThong;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QL_DichVu
{
    public partial class frmaddLoaiDV : DevExpress.XtraEditors.XtraForm
    {
        private DatabaseDataContext db;
        private LoaiDV obj;
        public int? ID { get; set; }
        public frmaddLoaiDV()
        {
            InitializeComponent();
            db = new DatabaseDataContext();
            obj = new LoaiDV();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (ID == null)
            {
                if (checkvali_null()) return;
                obj.MaLoaiDV = txtMaLoaiDV.Text;
                obj.TenLoaiDV = txtTenLoaiDV.Text;
                db.LoaiDVs.InsertOnSubmit(obj);
                try
                {
                    db.SubmitChanges();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
            else
            {
                obj.MaLoaiDV = txtMaLoaiDV.Text;
                obj.TenLoaiDV = txtTenLoaiDV.Text;
                try
                {
                    db.SubmitChanges();
                }
                catch (Exception)
                {
                    MessageBox.Show("LỖI DATABASE");
                }
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
        }

      
        private bool checkvali_null()
        {
            if (String.IsNullOrEmpty(txtMaLoaiDV.Text))
            {
                MessageBox.Show("Mã dịch vụ. xin mời nhập !", "Thông báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                return true;
            }
            if (String.IsNullOrEmpty(txtTenLoaiDV.Text))
            {
                MessageBox.Show("Tên dịch vụ. xin mời nhập !", "Thông báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                return true;
            }
            return false;
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void frmaddLoaiDV_Load(object sender, EventArgs e)
        { 
            if (ID != null)
            {
                obj = db.LoaiDVs.Single(p => p.ID == ID);
                txtTenLoaiDV.Text = obj.TenLoaiDV;
                txtMaLoaiDV.Text = obj.MaLoaiDV;
            }
        }
    }
}
