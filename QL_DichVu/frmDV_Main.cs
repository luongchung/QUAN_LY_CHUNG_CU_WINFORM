﻿using HeThong;
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace QL_DichVu
{
    public partial class frmDV_Main : Form
    {
        DatabaseDataContext db;
        public frmDV_Main()
        {
            InitializeComponent();
            db = new DatabaseDataContext();
        }
        private void load()
        {
            var data = (from a in db.DichVus
                        select new
                        {
                            a.ID,
                            a.TenDV,
                            a.MaDV,
                            a.Gia,
                            LoaiDV=(from b in db.LoaiDVs where a.IDLoaiDV==b.ID select b.TenLoaiDV).ToList().Single()
                        }).ToList();
            gcMain.DataSource = data;
        }
        private void btnNap_ItemClick_1(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            load();
        }

      



        private void frmLoaiDV_Main_Load(object sender, EventArgs e)
        {
            load();
            MenuSub.SetPopupContextMenu(gcMain, popupMenu);
        }

        private void btnSua_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var id = (int?)gvMain.GetFocusedRowCellValue("ID");
            if (id == null)
            {
                MessageBox.Show("Bạn chưa chọn vào hàng cần sửa, vui lòng chọn.");
                return;
            }
            var f = new frmaddDV();
            f.ID = id;
            f.ShowDialog();
            if (f.DialogResult == System.Windows.Forms.DialogResult.OK)
                load();
        }

        private void btnXoa_ItemClick_1(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gvMain.GetFocusedRowCellValue("ID") == null)
            {
                Thongbao.Hoi("Mời bạn chọn hàng cần xóa.");
            }
            else
            {
                int id_tmp = (int)gvMain.GetFocusedRowCellValue("ID");
                DialogResult f = Thongbao._CauHoi();
                if (f == System.Windows.Forms.DialogResult.Yes)
                {
                    var delete = (from a in db.DichVus where a.ID == (int)id_tmp select a).Single();
                    db.DichVus.DeleteOnSubmit(delete);
                    try { db.SubmitChanges(); }
                    catch (Exception) { MessageBox.Show("Xóa không thành công, vui lòng kiểm tra lại."); }
                    load();
                }
            }
        }

        private void btnNapDL_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            load();
        }
    }
}
