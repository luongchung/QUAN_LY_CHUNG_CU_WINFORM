﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Linq;
using HeThong;
namespace QL_DichVu
{
    public partial class rptBangGia : DevExpress.XtraReports.UI.XtraReport
    {
        DatabaseDataContext db;
        public rptBangGia()
        {
            InitializeComponent();
            db = new DatabaseDataContext();
            //binding
            xrSTT.DataBindings.Add(new XRBinding("Text", null, "STT"));
            xrTenDV.DataBindings.Add(new XRBinding("Text", null, "TenDV"));
            xrGia.DataBindings.Add(new XRBinding("Text", null, "Gia", "{0:##.###}"));

            var wait = HeThong.Thongbao.Loading();
            try
            {
                this.DataSource = (from a in db.DichVus select new { a.TenDV,a.Gia}).ToList();
                              
            }
            catch { }
            finally
            {
                wait.Close();
                wait.Dispose();
            }

        }

    }
}
