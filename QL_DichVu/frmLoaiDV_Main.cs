﻿using HeThong;
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace QL_DichVu
{
    public partial class frmLoaiDV_Main : Form
    {
        DatabaseDataContext db;
        public frmLoaiDV_Main()
        {
            InitializeComponent();
            db = new DatabaseDataContext();
        }
        private void load()
        {
            var data = (from a in db.LoaiDVs
                        select new
                        {
                            a.ID,
                            a.MaLoaiDV,
                            a.TenLoaiDV
                        }).ToList();
            gcMain_LoaiDV.DataSource = data;
        }
        private void btnNap_ItemClick_1(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            load();
        }

        private void btnXoa_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gvMain_LoaiDV.GetFocusedRowCellValue("ID") == null)
            {
                Thongbao.Hoi("Mời bạn chọn hàng cần xóa.");
            }
            else
            {
                int id_tmp = (int)gvMain_LoaiDV.GetFocusedRowCellValue("ID");
                DialogResult f = Thongbao._CauHoi();
                if (f == System.Windows.Forms.DialogResult.Yes)
                {
                    var delete = (from a in db.LoaiDVs where a.ID == (int)id_tmp select a).Single();
                    db.LoaiDVs.DeleteOnSubmit(delete);
                    try { db.SubmitChanges(); }
                    catch (Exception) { MessageBox.Show("Xóa không thành công, vui lòng kiểm tra lại."); }
                    load();
                }
            }
        }

        private void btnThem_ItemClick_1(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var f = new frmaddLoaiDV();
            f.ID = null;
            f.ShowDialog();
            if (f.DialogResult == System.Windows.Forms.DialogResult.OK)
                load();
        }

        private void btnSua_ItemClick_1(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var id = (int?)gvMain_LoaiDV.GetFocusedRowCellValue("ID");
            if (id == null)
            {
                MessageBox.Show("Bạn chưa chọn vào hàng cần sửa, vui lòng chọn.");
                return;
            }
            var f = new frmaddLoaiDV();
            f.ID = id;
            f.ShowDialog();
            if (f.DialogResult == System.Windows.Forms.DialogResult.OK)
                load();
        }

        private void frmLoaiDV_Main_Load(object sender, EventArgs e)
        {
            load();
        }
    }
}
