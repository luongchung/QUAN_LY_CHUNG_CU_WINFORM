﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HeThong;

namespace QL_DanCu
{
    public partial class frmAddDanCu : DevExpress.XtraEditors.XtraForm
    {
        public int? ID { get; set; }
        private DatabaseDataContext db;
        private DanCu obj;
        public frmAddDanCu()
        {
            InitializeComponent();
            db = new DatabaseDataContext();
            obj = new DanCu();
        }
        private bool checkvali_null()
        {
            if (String.IsNullOrEmpty(txtMaDC.Text))
            {
                MessageBox.Show("Mã dân cư. xin mời nhập !", "Thông báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                return true;
            }
            if (String.IsNullOrEmpty(txtTenDC.Text))
            {
                MessageBox.Show("Tên dân cư. xin mời nhập !", "Thông báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                return true;
            }
            return false;
        }
        private void btnLuu_Click_1(object sender, EventArgs e)
        {
            if (ID == null)
            {
                if (checkvali_null()) return;
                obj.MaDC = txtMaDC.Text;
                obj.HVT = txtTenDC.Text;
                obj.NgheNghiep = txtNgheNghiep.Text;
                obj.TonGiao = txtTonGiao.Text;
                obj.CMND = txtCMND.Text;

                if (lueDanToc.EditValue != null)
                    obj.IDDanToc = (int)lueDanToc.EditValue;
                if (txtNgaySinh.EditValue != null)
                    obj.NgaySinh = (DateTime)txtNgaySinh.EditValue;
                if (lueGioiTinh.EditValue != null)
                    obj.GioiTinh =(int) lueGioiTinh.EditValue;
                db.DanCus.InsertOnSubmit(obj);
                try
                {
                    db.SubmitChanges();
                }
                catch (Exception)
                {
                    MessageBox.Show("LỖI DATABASE");
                }
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
            else
            {
                obj.MaDC = txtMaDC.Text;
                obj.HVT = txtTenDC.Text;

                obj.NgheNghiep = txtNgheNghiep.Text;
                obj.TonGiao = txtTonGiao.Text;
                obj.CMND = txtCMND.Text;
              
                if (lueDanToc.EditValue != null)
                    obj.IDDanToc = (int)lueDanToc.EditValue;
                if (txtNgaySinh.EditValue != null)
                    obj.NgaySinh = (DateTime)txtNgaySinh.EditValue;
                if (lueGioiTinh.EditValue != null)
                    obj.GioiTinh = (int)lueGioiTinh.EditValue;
                try
                {
                    db.SubmitChanges();
                }
                catch (Exception)
                {
                    MessageBox.Show("LỖI DATABASE");
                }
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
        }
        private void btnDong_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }
        private void frmAddDanCu_Load(object sender, EventArgs e)
        {
            lueGioiTinh.Properties.DataSource = (from a in db.GioiTinhs select new { a.ID, a.GT }).ToList();
            lueDanToc.Properties.DataSource = (from a in db.DanTocs select new { a.ID, a.TenDanToc }).ToList();
            if (ID != null)
            {
                obj = db.DanCus.Single(p => p.ID == ID);
                txtMaDC.Text = obj.MaDC;
                txtTenDC.Text = obj.HVT;
                txtCMND.Text = obj.CMND;
                txtNgaySinh.EditValue = (DateTime)obj.NgaySinh;
                txtNgheNghiep.Text = obj.NgheNghiep;
                txtTonGiao.Text = obj.TonGiao;
                lueGioiTinh.EditValue = obj.GioiTinh;
                lueDanToc.EditValue = obj.DanToc;
            }
        }
    }
}
