﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HeThong;
namespace QL_DanCu
{
    public partial class frmMain : Form
    {
        DatabaseDataContext db;
        public frmMain()
        {
            InitializeComponent();
            db = new DatabaseDataContext();
        }
        private void btnThem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var f = new frmAddDanCu();
            f.ID = null;
            f.ShowDialog();
            if (f.DialogResult == System.Windows.Forms.DialogResult.OK)
                load();
        }

        private void btnSua_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var id = (int?)gvMain.GetFocusedRowCellValue("ID");
            if (id == null)
            {
                MessageBox.Show("Bạn chưa chọn vào hàng cần sửa, vui lòng chọn.");
                return;
            }
            var f = new frmAddDanCu();
            f.ID = id;
            f.ShowDialog();
            if (f.DialogResult == System.Windows.Forms.DialogResult.OK)
                load();
        }

        private void btnXoa_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gvMain.GetFocusedRowCellValue("ID") == null)
            {
                Thongbao.Hoi("Mời bạn chọn hàng cần xóa.");
            }
            else
            {
                int id_tmp = (int)gvMain.GetFocusedRowCellValue("ID");
                DialogResult f = Thongbao._CauHoi();
                if (f == System.Windows.Forms.DialogResult.Yes)
                {
                    var delete = (from a in db.DanCus where a.ID == (int)id_tmp select a).Single();
                    db.DanCus.DeleteOnSubmit(delete);
                    try { db.SubmitChanges(); }
                    catch (Exception) { MessageBox.Show("Xóa không thành công, vui lòng kiểm tra lại."); }
                    load();
                }
            }
        }

        private void btnNap_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            load();
        }
        private void btnExport_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                gcMain.ExportToXlsx(@"C:\Users\LUONG CHUNG\Downloads");
            }
            catch
            {

            }
        }
        private void load()
        {
            var data = (from a in db.DanCus select new {
                a.ID,
                a.MaDC,
                a.HVT,
                GioiTinh=(a.GioiTinh==1)?"Nam":"Nữ",
                TenDanToc=(from b in db.DanTocs where a.IDDanToc==b.ID select b.TenDanToc).Single(),
                a.NgaySinh,
                a.NgheNghiep,
                a.TonGiao,
                a.CMND
            }).ToList();
            gcMain.DataSource = data;
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            load();
        }
    }
}
